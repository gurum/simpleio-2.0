/*
 * io.h
 *
 *  Created on: Oct 18, 2018
 *      Author: buttonfly
 */

#ifndef GURUM_IO_H_
#define GURUM_IO_H_

#include <unistd.h>

namespace gurum {

static int Write(int fd, uint8_t *buf, size_t len) {
  int off=0;
  for(int n = 0; off < len; off += n) {
    n = write(fd,(uint8_t *) buf + off, len - off);
    if(n<=0) break;
  }
  return off;
}

static int Read(int fd, uint8_t *buf, size_t len) {
  int off = 0;
  for(int n = 0; off < len; ) {
    n = read(fd, (uint8_t *)buf + off, len - off);
    if(n < 0) {
      return n;
    }
    else if(n == 0) { // weird... dig into it later
      return n;
    }
    off += n;
  }
  return off;
}

}

#endif /* GURUM_IO_H_ */
