/*
 * MulticastServer.cc
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#include "multicast_server.h"


#define UDP_HDR_SIZE 8
#define RTP_HDR_SIZE 12

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
//#include <linux/if_ether.h>
#include <net/if_arp.h>
//#include <netpacket/packet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include "netut.h"
#include <assert.h>

namespace gurum {

MulticastServer::MulticastServer()
: ttl_(3), loop_(1), port_(0)
{
}

MulticastServer::~MulticastServer() {
	// TODO Auto-generated destructor stub
}

int MulticastServer::Open() {

	if(port_ == 0) {
		fprintf(stderr, "port is not set.\n");
		return -1;
	}

	if(ip_.empty()) {
		fprintf(stderr, "ip is not set.\n");
		return -1;
	}

	fprintf(stderr, "%s:%d\n", ip_.c_str(), port_);

	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	assert(fd > 0);

	memset(&server_addr_, 0, sizeof(server_addr_));
	const char *ip = ip_.c_str(); // guard:  is multicast?
	server_addr_.sin_family = PF_INET;
	server_addr_.sin_port = htons(port_); // Use the first free port
	server_addr_.sin_addr.s_addr = inet_addr(ip); // bind socket to any interface

	if(! NetUt::isMulticast((const sockaddr_storage *) &server_addr_, sizeof(server_addr_))) {
		fprintf(stderr, "%s is invalid\n", ip_.c_str());
		goto exception;
	}

	if(::bind(fd, (struct sockaddr *)&server_addr_, sizeof(struct sockaddr_in)) < 0) {
		// TODO
		fprintf(stderr, "failed to bind\n");
		goto exception;
	}

	if(alias_.empty()) {
		in_addr_.s_addr = NetUt::defaultIP(); // @deprecated
	}
	else {
		in_addr_.s_addr = NetUt::IP(alias_.c_str());
	}

	fprintf(stderr, "ip: %x\n", in_addr_.s_addr);
	setsockopt(fd, IPPROTO_IP, IP_MULTICAST_IF, &in_addr_, sizeof(struct in_addr));
	setsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL, &ttl_, sizeof(unsigned char));
	setsockopt(fd, IPPROTO_IP, IP_MULTICAST_LOOP,&loop_, sizeof(unsigned char));
	return fd;

exception:
	close(fd);
	return -1;
}

void MulticastServer::Run(int clnt)  {
	// TODO:
//	fprintf(stderr, "*");
	sleep(1);
}

int MulticastServer::Send(uint8_t *buf, size_t len) {
	int addrlen = SockaddrLen();
	return sendto(sck_, buf, len, 0,(struct sockaddr *)&server_addr_, addrlen);
}

struct sockaddr *MulticastServer::Sockaddr() {
	return (struct sockaddr *) &clnt_addr_;
}

size_t MulticastServer::SockaddrLen() {
	return sizeof(struct sockaddr_in);
}

} /* namespace gurum */


void *multicast_server_new(){
  gurum::MulticastServer *server = new gurum::MulticastServer;
  return server;
}

void multicast_server_set_ip(void *handle, const char *addr){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetIP(addr);
}

void multicast_server_set_port(void *handle, uint16_t port){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetPort(port);
}

void multicast_server_set_alias(void *handle, const char *alias){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetAlias(alias);
}

void multicast_server_set_ttl(void *handle, uint8_t ttl){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetTTL(ttl);
}

void multicast_server_set_loop(void *handle, uint8_t loop){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetLoop(loop);
}

int multicast_server_start(void *handle){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  return server->Start();
}

void multicast_server_stop(void *handle){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->Stop();
}

void multicast_server_delete(void *handle){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  delete server;
}

void multicast_server_set_connected_callback(void *handle,std::function<void(int)> cb){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetConnectedCallback(cb);
}

void multicast_server_set_disconnected_callback(void *handle,std::function<void(int)> cb){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetDisconnectedCallback(cb);
}

void multicast_server_set_read_callback(void *handle,std::function<void(int,uint8_t*,int)> cb){
  gurum::MulticastServer *server = static_cast<gurum::MulticastServer *>(handle);
  server->SetReadCallback(cb);
}


