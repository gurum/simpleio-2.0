/*
 * SimplePipeClient.cc
 *
 *  Created on: Feb 11, 2017
 *      Author: buttonfly
 */

#include "simple_client.h"
#include <unistd.h>
#include <fcntl.h>

namespace gurum {

SimplePipeClient::SimplePipeClient() // default: stdin
{
}

SimplePipeClient::~SimplePipeClient() {
	// TODO Auto-generated destructor stub
}

int SimplePipeClient::Open() {

	if(! path_.empty()) {
#if __linux__
		sck_ = ::open(path_.c_str(), O_RDONLY);
		if(sck_ < 0) {
			fprintf(stderr, "failed to open: %s\n", path_.c_str());
			return -1;
		}
#endif
	}
	else {
		sck_ = 0; // default: stdin
	}
	return sck_;
}

} /* namespace gurum */
