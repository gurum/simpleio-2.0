/*
 * SimpleServer.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include "simple_server.h"

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/select.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <condition_variable>
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#define PID_PATH "/tmp/"
#include "log_message.h"
#include "simpleio/types.h"
#include "io.h"

using namespace std;

namespace gurum {

//using Buffer=std::unique_ptr<uint8_t, std::function<void(uint8_t *)>>;

int SimpleServer::kSimpleProtocolHeaderSize=sizeof(struct simple_protocol_header);

SimpleServer::SimpleServer()
{
}

SimpleServer::~SimpleServer() {
	if(! stopped_) Stop();
}

int SimpleServer::Start() {
	sck_ = Open();
	if(sck_ < 0) {
		fprintf(stderr, "failed to open a socket\n");
		return -1;
	}

	fd_max_ = sck_;

	FD_ZERO(&readfds_);
	FD_SET(sck_, &readfds_);

	thread_ = std::thread([&](){
		while(!stopped_) Run(0);
	});
	started_ = true;
	return 0;
}

int SimpleServer::Send(uint8_t *buf, size_t len) {
  fd_set tmpfds;
  tmpfds = readfds_;

  int n = 0;
  for(int fd=0; fd < fd_max_ + 1; fd++) {
    if(! FD_ISSET(fd, &tmpfds)) continue;

    if(fd==sck_) continue;

    n = Send(fd, buf, len);
  }
  return n;
}

int SimpleServer::Send(const std::string &msg) {
  const char *tmp = msg.c_str();
  return Send((uint8_t *) tmp, (int) msg.size());
}

int SimpleServer::Send(int clnt, uint8_t *buf, size_t len) {
  std::lock_guard<std::recursive_mutex> lk(lck_);
//  std::lock_guard<std::mutex> lk(lck_);

  fd_set tmpfds;
  tmpfds = readfds_;

  if(! FD_ISSET(clnt, &tmpfds)) {
    LOG(ERROR) << __func__ << " invalid client : " << clnt;
    return -1;
  }

  struct simple_protocol_header header = {0,};
  header.version = simple_protocol_header::V1;
  header.len = len;
//  header.id = ++seq_;
  header.crc32 = crc32((uint8_t *) buf, len);

  int n;
  n = Write(clnt, (uint8_t *)&header, kSimpleProtocolHeaderSize);
  if(n!=kSimpleProtocolHeaderSize) {
    LOG(WARNING) << __func__ << " failed to write all.";
  }

  n = Write(clnt, buf, len);
  return n;
}

int SimpleServer::Send(int clnt, const std::string &msg) {
  return Send(clnt, (uint8_t *)msg.c_str(), msg.size());
}

int SimpleServer::ReadAndDispatch() {
  if(stopped_)
    return -1;

  if(sck_ < 0) {
    sck_ = Open();
    if(sck_ < 0) return -1;

    fd_max_ = sck_;

    FD_ZERO(&readfds_);
    FD_SET(sck_, &readfds_);
  }

  Run(0);
  return 0;
}

void SimpleServer::Run(int unused) {


	int clntsck;
	fd_set tmpfds;
	int clntaddr_len = SockaddrLen();
	tmpfds = readfds_;

	struct timeval tv;
	struct timeval *p_tv=NULL;

	if(timeout_ > 0) {
		tv.tv_sec = timeout_ / 1000;
		tv.tv_usec = (timeout_ % 1000) * 1000;
		p_tv = &tv;
	}
//	_timeout
	int stat;
   	do
   		stat = select(fd_max_ + 1, &tmpfds, NULL, NULL, p_tv);
   	while (EINTR == errno);

	if(stat == 0) { // time-over
	}

	for(int fd=0; fd < fd_max_ + 1; fd++) {
		if(! FD_ISSET(fd, &tmpfds)) continue;

		if(fd == sck_) {
			clntsck = accept(fd, Sockaddr(), (socklen_t*) &clntaddr_len);
			if(clntsck < 0) continue;

			if(clntsck > fd_max_) fd_max_ = clntsck;

			FD_SET(clntsck, &readfds_);
			if(on_connected_) on_connected_(clntsck);

			// @deprecated
			// if(delegate_) delegate_->connected(clntsck);
		}
		else {
			int n;
			if(ioctl(fd, FIONREAD, &n) < 0) continue;

			if(n==0) {
				FD_CLR(fd, &readfds_);
				if(on_disconnected_) on_disconnected_(fd);

        // @deprecated
				// if(delegate_) delegate_->disconnected(fd);
			}
			else if(n > kSimpleProtocolHeaderSize) {

	      struct simple_protocol_header header = {};
	      int err = 0;
	      err = ReadHead(fd, header);
	      if(err) continue;

	      Buffer buf = ReadBody(fd, header);
	      if(buf && on_read_) on_read_(fd, (uint8_t *)buf.get(), n);

				// @deprecated
				// if(delegate_) delegate_->read(fd, (uint8_t *)buf.get(), n);

			}
		}
	}
}

int SimpleServer::ReadHead(int fd, struct simple_protocol_header &header) {
  int n = 0;
  n = Read(fd, (uint8_t *)&header, kSimpleProtocolHeaderSize);
  if(n != kSimpleProtocolHeaderSize) {
    LOG(ERROR) << __func__ << " expected value: " << kSimpleProtocolHeaderSize << " but " << n;
    LOG(WARNING) << Hexdump((uint8_t *) &header, n);
    return -1;
  }

  if(header.version!=simple_protocol_header::V1) {
    LOG(ERROR) << __func__ << " header.version!=simple_protocol_header::V1";
    LOG(WARNING) << Hexdump((uint8_t *) &header, n);
    return -1;
  }

  if((header.len > 1000) || (header.len < 0)){
    LOG(WARNING) << " It might be an unexpected value.";
    LOG(WARNING) << Hexdump((uint8_t *) &header, n);
    return -1;
  }

  return 0;
}

Buffer SimpleServer::ReadBody(int fd, const struct simple_protocol_header &header) {

  int n;
  Buffer buf{
      (uint8_t *) malloc(header.len + 1),
      [](uint8_t *ptr) { if(ptr) { free(ptr);}}
  };
  if(!buf) {
    LOG(ERROR) << __func__ << " failed to allocate memory";
    return nullptr;
  }

  memset(buf.get(), 0, header.len + 1);

  ioctl(fd, FIONREAD, &n);
  if(n < header.len) {
    LOG(WARNING) << " " << header.len << " is expected but : " << n;
  }

  n = Read(fd, buf.get(), header.len);
  if(n != header.len) {
    LOG(ERROR) << __func__ << " n != header.len";
  }

  unsigned long crc = crc32(buf.get(), header.len);
  if(crc != header.crc32) {
    LOG(WARNING) << __func__ << " crc is incorrect. This message will be dropped off.";
    LOG(WARNING) << Hexdump((uint8_t *)buf.get(), n);
    return nullptr;
  }

  return std::move(buf);
}

void SimpleServer::Stop() {
	if(thread_.native_handle() == pthread_self()) {
		fprintf(stderr, "W: This thread is not allowed to call this method\n");
		return;
	}

	if(stopped_) {
		fprintf(stderr, "W: It's already stopped.\n");
		return;
	}

	stopped_ = true;

	if(started_) {
		thread_.join();
	}

	// no thread for now.
	if(sck_ > 0) close(sck_);
	sck_ = -1;
}

// remove soon
void SimpleServer::lock() {
	lck_.lock();
}

void SimpleServer::unlock() {
	lck_.unlock();
}



} /* namespace gurum */
