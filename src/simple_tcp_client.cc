/*
 * SimpleTcpClient.cc
 *
 *  Created on: May 3, 2016
 *      Author: buttonfly
 */




#include "simple_client.h"
#include <sys/un.h>
#include <sys/socket.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "simpleio/netut.h"

namespace gurum {

SimpleTcpClient::SimpleTcpClient()
: port_(0)
{
}

SimpleTcpClient::~SimpleTcpClient() {
}


int SimpleTcpClient::Open() {
	int 	fd;
	struct sockaddr_in server_addr;
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(fd < 0) {
		return -1;
	}

	if(!alias_.empty()) {
	  struct sockaddr_in client_addr;
	  bzero(&client_addr, sizeof(client_addr));
	  client_addr.sin_family = AF_INET;
	  client_addr.sin_addr.s_addr = NetUt::IP(alias_);
	  if(::bind(fd, (struct sockaddr*) &client_addr, sizeof(client_addr)) < 0) {
	    fprintf(stderr, "failed to bind\n");
	  }
	  std::string  tmp = NetUt::SockAddrToString((const struct sockaddr *)&client_addr, sizeof(client_addr));
	  fprintf(stderr, "bind done!!!!!!!!!!!! %s\n",tmp.c_str());
	}

	bzero(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port_);
	if(!addr_.empty()) {
	  server_addr.sin_addr.s_addr = inet_addr(addr_.c_str());
	}

	int ret = ::connect(fd,(struct sockaddr *) &server_addr, sizeof(server_addr));
	if(ret == -1) {
		goto exception;
	}

	if(on_connected_) on_connected_();

	if(delegate_) delegate_->connected();
	return fd;

exception:
	close(fd);
	return -1;
}

}  /* namespace gurum */
