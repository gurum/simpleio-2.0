/*
 * SimpleClient.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include "simple_client.h"

#include <sys/un.h>
#include <sys/socket.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <condition_variable>

#include "log_message.h"
#include "simpleio/types.h"
#include "io.h"

namespace gurum {

int SimpleClient::kSimpleProtocolHeaderSize=sizeof(struct simple_protocol_header);

SimpleClient::SimpleClient()
: stopped_(false), delegate_(nullptr), sck_(-1), timeout_(0)
{
}

SimpleClient::~SimpleClient() {
	if(! stopped_) Stop();
}

int SimpleClient::Start() {
	sck_ = Open();
	if(sck_ <= 0) {
	  LOG(ERROR) << " failed to open socket: " << sck_;
	  return  sck_;
	}

	thread_ = std::thread([&](){
		for(; ! stopped_ ;) Run();
	});

  started_ = true;
	return 0;
}

void SimpleClient::Stop() {
  if(thread_.native_handle() == pthread_self()) {
    LOG(ERROR) << __func__ << " This thread is not allowed to call this method";
    return;
  }

  if(stopped_) return;

	stopped_ = true;
	if(started_) thread_.join();
	if(sck_ > 0)
		close(sck_);
	sck_ = -1;
}

int SimpleClient::ReadAndDispatch() {
  if(stopped_)
    return -1;

  if(sck_ < 0)
    sck_ = Open();

  if(sck_ < 0) return -1;

  Run();
  return 0;
}

void SimpleClient::Run() {

	fd_set readfds;
	struct timeval tv = { 0 };
	struct timeval *p_tv = NULL;

	assert(sck_ >= 0);
	FD_ZERO(&readfds);
	FD_SET(sck_, &readfds);

	if(timeout_ > 0) {
		tv.tv_sec = timeout_ / 1000;
		tv.tv_usec = (timeout_ % 1000) * 1000;
		p_tv = &tv;
	}

	int stat = select(sck_+1, &readfds, NULL, NULL, p_tv);
	if(stat == 0) { // time-over
	}
	else if (0 < stat) {
		int n;
		if(ioctl(sck_, FIONREAD, &n) < 0)
			return;

		if(n==0) {
			FD_CLR(sck_, &readfds);
      if(on_disconnected_) {
        stopped_ = true;
        on_disconnected_();
      }

      //@deprecated
			// if(delegate_) delegate_->disconnected();

		}
		else if(n > kSimpleProtocolHeaderSize) {
		  if(on_hook_ && on_hook_(sck_, n)) return;

      struct simple_protocol_header header = {};
      int err = 0;
      err = ReadHead(header);
      if(err) {
        return;
      }

      Buffer buf = ReadBody(header);


      if(buf && on_read_) on_read_((uint8_t *)buf.get(), n);



#if deprecated
      // if(buf) free(buf);

      // @deprecated
			if(delegate_) {
				// if returning true,
				// It means that a user will read for himself.
				if(delegate_->hook(sck_, n)) return;

				uint8_t *buf = (uint8_t *) malloc(n + 1);
				memset(buf, 0, n + 1);
				assert(buf);
				n = this->Recv(sck_, buf, n);

				if(delegate_) delegate_->read((uint8_t *)buf, n);

				if(buf) free(buf);
			}
#endif
		}
	}
	/* Else it timed out */
	//    printf("\nExiting : %s\n", __func__);
}

int SimpleClient::Send(void *buf, int len) {
  std::lock_guard<std::mutex> lk(lck_);

  int n;
  struct simple_protocol_header header;
  header.version = simple_protocol_header::V1;
  header.len = len;
  header.crc32 = crc32((uint8_t *) buf, len);

  n = Write(sck_, (uint8_t*)&header, kSimpleProtocolHeaderSize);
  assert(n==kSimpleProtocolHeaderSize);
  // TODO:

  n = Write(sck_, (uint8_t*)buf, len);
  assert(n==len);
  // TODO:
  return n;
}

int SimpleClient::Send(const string &msg) {
	const char *tmp = msg.c_str();
	return Send((void *) tmp, (int) msg.size());
}
//
//int SimpleClient::Send(const char *msg) {
//	return Send((void *) msg, strlen(msg));
//}

int SimpleClient::Recv(int fd, uint8_t *buf, size_t len) {
	return read(fd, buf, len);
}

int SimpleClient::ReadHead(struct simple_protocol_header &header) {
  int n = 0;
  n = Read(sck_, (uint8_t *)&header, kSimpleProtocolHeaderSize);
  if(n != kSimpleProtocolHeaderSize) {
    LOG(ERROR) << __func__ << " expected value: " << kSimpleProtocolHeaderSize << " but " << n;
    LOG(WARNING) << Hexdump((uint8_t *) &header, n);
    return -1;
  }

  if(header.version!=simple_protocol_header::V1) {
    LOG(ERROR) << __func__ << " header.version!=simple_protocol_header::V1";
    LOG(WARNING) << Hexdump((uint8_t *) &header, n);
    return -1;
  }

  if((header.len > 1000) || (header.len < 0)){
    LOG(WARNING) << " It might be an unexpected value.";
    LOG(WARNING) << Hexdump((uint8_t *) &header, n);
    return -1;
  }

  return 0;
}

Buffer SimpleClient::ReadBody(const struct simple_protocol_header &header) {

  int n;
  Buffer buf{
      (uint8_t *) malloc(header.len + 1),
      [](uint8_t *ptr) { if(ptr) { free(ptr);}}
  };
  if(!buf) {
    LOG(ERROR) << __func__ << " failed to allocate memory";
    return nullptr;
  }

  memset(buf.get(), 0, header.len + 1);

  ioctl(sck_, FIONREAD, &n);
  if(n < header.len) {
    LOG(WARNING) << " " << header.len << " is expected but : " << n;
  }

  n = Read(sck_, buf.get(), header.len);
  if(n != header.len) {
    LOG(ERROR) << __func__ << " n != header.len";
  }

  unsigned long crc = crc32(buf.get(), header.len);
  if(crc != header.crc32) {
    LOG(WARNING) << __func__ << " crc is incorrect. This message will be dropped off.";
    LOG(WARNING) << Hexdump((uint8_t *)buf.get(), n);
    return nullptr;
  }

  return std::move(buf);
}


} /* namespace gurum */
