/*
 * SimplePipeServer.cc
 *
 *  Created on: Feb 11, 2017
 *      Author: buttonfly
 */

#include "simple_server.h"
#include <unistd.h>
#include <fcntl.h>
#if __linux__
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <limits.h>
#include <signal.h>
#include <assert.h>
#endif

namespace gurum {

SimplePipeServer::SimplePipeServer() {
}

SimplePipeServer::~SimplePipeServer() {
}

int SimplePipeServer::Open() {

	if(!path_.empty()) {
		//
#if __linux__
		unlink(path_.c_str());
		int err = mkfifo(path_.c_str(), 0666);
		if(err) {
			fprintf(stderr, "E: failed to mkfifo %s\n", path_.c_str());
			return -1;
		}

		fd_ = ::open(path_.c_str(), O_RDWR);
		if(fd_ < 0) {
			fprintf(stderr, "E: failed open : %s\n", path_.c_str());
		}

		id_ = inotify_init();
		assert(id_>0);
		wd_ = inotify_add_watch(id_, path_.c_str(), IN_OPEN|IN_CLOSE);
		assert(wd_>0);
#endif
	}
	return fd_;
}

void SimplePipeServer::Stop() {
	SimpleServer::Stop();

#ifdef __linux__
	inotify_rm_watch(id_, wd_);
	close(id_);
#endif
	if(! path_.empty()) {
		::unlink(path_.c_str());
		path_ = "";
	}
}

void SimplePipeServer::Run(int clnt) {
#ifdef __linux__
#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))
	int n;
	char buf[BUF_LEN] __attribute__ ((aligned(8)));
	n = read(id_, buf, BUF_LEN);

	if(n==0) {
		// error
	}

	if(n==-1) {
		// error
	}


	char *ptr;
	struct inotify_event *event;
	for(ptr = buf; ptr < buf + n; ptr += sizeof(struct inotify_event) + event->len) {
		event = (struct inotify_event *) ptr;
		// TODO
		if (event->mask & IN_CLOSE_NOWRITE) {
			if(delegate_)
			  delegate_->disconnected(0);
		}
		if (event->mask & IN_OPEN)   {
			if(delegate_)
			  delegate_->connected(0);
		}
	}
#endif
}

int SimplePipeServer::Send(uint8_t *buf, size_t len) {
	return write(fd_, buf, len);
}


} /* namespace gurum */
