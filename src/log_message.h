
#ifndef GURUM_BASE_LOGGING_LOG_MESSAGE_H_
#define GURUM_BASE_LOGGING_LOG_MESSAGE_H_

#if defined(USE_GLOG)
#include <glog/logging.h>
#elif defined(USE_SYSLOG)

#if defined(LOG)
#undef LOG
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sstream>
#include <ostream>
#include <syslog.h>
#include <string.h>
#include <pthread.h>
#include <chrono>

typedef int LogSeverity;
const LogSeverity VERBOSE = -1;
const LogSeverity INFO = LOG_INFO;
const LogSeverity WARNING = LOG_WARNING;
const LogSeverity ERROR = LOG_ERR;
const LogSeverity FATAL = LOG_ERR;

namespace base {

namespace logging {

class LogMessage {
public:
  LogMessage(LogSeverity severity, const std::string &file, const std::string &func, int line)
  : severity_(severity), file_(file), func_(func), line_(line) {
    static auto start = std::chrono::high_resolution_clock::now();
    stream() << "[" << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count() << "]"
        << "[" << std::hex <<  (unsigned int) pthread_self() << "]" << std::dec;
  }

  ~LogMessage() {
    syslog(severity_, "%s",stream_.str().c_str());
  }

  std::ostream &stream() {return stream_;}

private:
  std::ostringstream stream_;
  LogSeverity severity_;
  const std::string file_;
  const std::string func_;
  const unsigned int line_;
};

} // namespace logging

} // namespace base

#define LOG(verbose_level) ::base::logging::LogMessage(verbose_level, __FILE__, __func__, __LINE__).stream()


#else


#if defined(LOG)
#undef LOG
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sstream>
#include <ostream>
#include <syslog.h>
#include <string.h>
#include <pthread.h>
#include <chrono>
#include <iomanip>
#include <assert.h>
#ifdef __APPLE__
#include <libgen.h>
#endif
#include <iostream>


typedef int LogSeverity;
const LogSeverity VERBOSE = -1;
const LogSeverity INFO = LOG_INFO;
const LogSeverity WARNING = LOG_WARNING;
const LogSeverity ERROR = LOG_ERR;
const LogSeverity FATAL = LOG_ERR;

namespace base {

namespace logging {

class LogMessage {
public:
  LogMessage(LogSeverity severity, const std::string &file, const std::string &func, int line)
  : severity_(severity), file_(basename(file.c_str())), func_(func), line_(line) {
    static auto start = std::chrono::high_resolution_clock::now();
    stream()
        << "["
        <<  std::setfill ('0') << std::setw (10)
        << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count()
        << "]"
        << "["
        <<  std::setfill ('0') << std::setw (10)
        << std::hex <<  (unsigned int) pthread_self()
        << "] " << std::dec
        << file_ << ":" <<line_ << "] ";
  }

  ~LogMessage() {
    std::cerr << stream_.str() << std::endl;
 }

  std::ostream &stream() {return stream_;}

private:
  std::ostringstream stream_;
  LogSeverity severity_;
  const std::string file_;
  const std::string func_;
  const unsigned int line_;
};

} // namespace logging

} // namespace base

#define LOG(verbose_level) ::base::logging::LogMessage(verbose_level, __FILE__, __func__, __LINE__).stream()
#define CHECK_NOTNULL(x) assert(x)

#endif



#include <sstream>
#include <ostream>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <cstring>

namespace {

static std::string Hexdump(const uint8_t *data, size_t len) {
  std::ostringstream stream;
  stream << std::hex << std::setfill ('0') << std::setw (2);

  for(int i=0; i < len; i++) {

    stream << (int) (0xFF & data[i]) << " ";

    if(((i+1)%0x10)==0) stream << "\n";
  }

  stream << std::dec << "\n";


  stream << " [";

  for(int i=0; i < len; i++) {
    char tmp = '.';
    if(((data[i] >= 'A') && (data[i] <= 'z')) || ((data[i] >= '0') && (data[i] <= '9')) || (data[i]==',')
        || (data[i]=='\"') || (data[i]==':') || (data[i]=='{') || (data[i]=='}')) {
      tmp = data[i];
    }
    stream << tmp << " ";

    if(((i+1)%0x10)==0) stream << "\n";
  }

  stream << " ]";

  return stream.str();
}

}
#endif // GURUM_BASE_LOGGING_LOG_MESSAGE_H_

