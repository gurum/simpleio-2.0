/*
 * SimpleTcpServer.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include "simple_server.h"
#include <stdio.h>

using namespace std;

namespace gurum {

SimpleTcpServer::SimpleTcpServer()
:port_(0)
{
}

SimpleTcpServer::~SimpleTcpServer() {
}

int SimpleTcpServer::Open() {
	 int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (fd  < 0) {
		fprintf(stderr, "failed to create a socket descriptor\n");
		return -1;
	}

	int enable = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));

	memset(&serveraddr_, 0, sizeof(serveraddr_));
	serveraddr_.sin_family = AF_INET;
	serveraddr_.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr_.sin_port = htons(port_);

	if(::bind(fd, (struct sockaddr*) &serveraddr_, sizeof(serveraddr_)) < 0) {
		fprintf(stderr, "failed to bind\n");
	}

	if(::listen(fd, 5) < 0) {
		fprintf(stderr, "failed to listen\n");
	}
	return fd;
}

struct sockaddr *SimpleTcpServer::Sockaddr()  {
	return (struct sockaddr *) &clntaddr_;
}

size_t SimpleTcpServer::SockaddrLen()  {
	return sizeof(struct sockaddr_in);
}

} /* namespace gurum */
