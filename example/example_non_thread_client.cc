/*
 * example_non_thread_client.cc
 *
 *  Created on: Oct 1, 2017
 *      Author: buttonfly
 */

#include <simpleio/simple_client.h>
#include <string>
#include <memory>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>

using namespace std;
using namespace gurum;

int main(int argc, char *argv[]) {

  unique_ptr<SimpleUnixClient> clnt{new SimpleUnixClient};
  clnt->SetTimeout(10);
  clnt->SetUnixSockPath(".sck");
  clnt->SetCallback([](){
    fprintf(stderr, "connected\n");
  },
  [&]() {
    fprintf(stderr, "disconnected\n");
    clnt->Stop();
  },
  [](int sck, int len)->bool{
    fprintf(stderr, "hook\n");
    return false; // if true, [read] wont' be called
  },
  [](uint8_t* buf, int len) {
    fprintf(stderr, "read: %s\n", buf);
  });

#if 1
  for(;;) {
    string c;
    clnt->ReadAndDispatch();
    getline(cin, c);
    clnt->Send((uint8_t *)c.c_str(), c.size());
  }
#else
  if(clnt->Start() == 0) {
    string c;
    for(;;) {
      getline(cin, c);
      clnt->Send(c);
    }
  }
#endif
  return 0;
}


