/*
 * example_pipe_client.cc
 *
 *  Created on: Aug 1, 2018
 *      Author: buttonfly
 */


#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

#include <thread>
#include <vector>
#include "log_message.h"


class Message {
public:
  enum action_t{
    ACTION_ENTER,
    ACTION_MOVE,
    ACTION_LEAVE
  };

  struct message_t {
    action_t action;
    int numof_points; // # of fingers
  };

  struct point_t {
    int x;
    int y;
  };

  explicit Message(int fd) : fd_(fd){
    LOG(INFO) << __func__;
    message_t m;
    int n;
    int len_to_read = sizeof(m);
    for(int offset=0, n=0; offset < len_to_read; offset +=n) {
      n = read(fd_, &m, len_to_read - offset);
    }

    action_ = m.action;

    for(int i = 0; i < m.numof_points; i++) {
      point_t point;
      len_to_read = sizeof(point);
      for(int offset=0, n=0; offset < len_to_read; offset +=n) {
        n = read(fd_, &m, len_to_read - offset);
      }
      points_.push_back(point);
    }
  }

  ~Message() {
  }

  action_t action() { return  action_;}
  const std::vector<point_t> &points() { return points_;}

private:
  const int fd_;
  action_t action_=ACTION_ENTER;
  std::vector<point_t> points_;
};



int main(int argc, char *argv[]) {
  int fd_;
  bool stopped_=false;
  std::thread thread_;

  const char fifo_path[] = "/tmp/simpleio/.simple_fifo";
  LOG(INFO) << __func__ << " open!!!!!!!!!!!!!";

  fd_ = open(fifo_path, O_RDONLY);
  LOG(INFO) << __func__ << " open!!!!!!!!!!!!!";

  if(fd_ > 0) {
   thread_=std::thread([&] {
     LOG(INFO) << " thread start!";
     while(!stopped_) {
       Message m(fd_);
       switch(m.action()) {
       case Message::ACTION_ENTER: {
         auto points= m.points();
         LOG(INFO) << __func__;
         break;
       }
       case Message::ACTION_MOVE: {
         auto points= m.points();
         LOG(INFO) << __func__;
         break;
       }
       case Message::ACTION_LEAVE: {
         auto points= m.points();
         LOG(INFO) << __func__;
         break;
       }
       } // switch
     }
   });
  }
  else {
   LOG(ERROR) << __func__;
  }

  getchar();
  stopped_=true;
  thread_.join();
  return 0;
}
