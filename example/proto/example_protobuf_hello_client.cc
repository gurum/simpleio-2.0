
#include <simpleio/simple_client.h>
#include <iostream>
#include <fstream>
#include <string>
#include "search.pb.h"
#include "log_message.h"
#include <mutex>
#include <condition_variable>

using namespace std;
using namespace std::placeholders;



using namespace gurum;

class Simple {
public:
  Simple(uint16_t port) {
    client->SetPort(port);
    client->SetOnRead(std::bind(&Simple::OnRead, this, _1, _2));
    client->SetTimeout(10);
    client->Start();
  }

  ~Simple() {

  }

  void OnRead(uint8_t *buf, int len) {
    LOG(INFO) << __func__;
    std::lock_guard<std::mutex> lk(lck_);
    que_.ParseFromArray(buf, len);
    cond_.notify_one();
  }


  const gurum::SearchResponse &Search(const gurum::SearchRequest &req) {
    std::unique_lock<std::mutex> lk(lck_);

    std::string serialized;
    req.SerializeToString(&serialized);
    LOG(INFO) << __func__ << " " << serialized;
    client->Send(serialized);

    cond_.wait(lk); // TODO timeout.

    return que_;
  }

private:
  std::unique_ptr<SimpleTcpClient> client{new SimpleTcpClient};
  std::mutex lck_;
  std::condition_variable cond_;
  gurum::SearchResponse que_; // TODO
};

int main(int argc, char *argv[]) {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  const uint16_t port = 9026;
  Simple simple(port);

  std::string c;
  for(;;) {
    std::getline(std::cin, c);
    SearchRequest req;
    req.set_greeting(c);
    const gurum::SearchResponse &res = simple.Search(req);
    const std::string &reply = res.reply();
    LOG(INFO) << __func__ << " " << reply;
  }

  google::protobuf::ShutdownProtobufLibrary();
  return 0;
}


