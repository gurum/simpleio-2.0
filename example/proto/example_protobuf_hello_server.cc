#include <simpleio/simple_server.h>
#include <iostream>
#include <cstring>
#include <iostream>
#include <sstream>

#include "search.pb.h"
#include "log_message.h"



using namespace gurum;

class Simple {
public:
  Simple(uint16_t port) {
    using namespace std::placeholders;

    server->SetPort(port);
    server->SetTimeout(10);
    server->SetOnConnected(std::bind(&Simple::OnConnected, this, _1));
    server->SetOnRead(std::bind(&Simple::OnRead, this, _1, _2, _3));
    server->Start();
  }

  ~Simple() {

  }

  void OnConnected(int sck) {
    LOG(INFO) << __func__;
  }

  void OnDisconnected(int sck) {
    LOG(INFO) << __func__;
  }

  void OnRead(int sck, uint8_t *buf, int len) {
    LOG(INFO) << __func__;
    gurum::SearchRequest req;
    req.ParseFromArray(buf, len);
    const std::string &greeting = req.greeting();
    LOG(INFO) << __func__ << " " << greeting;


    gurum::SearchResponse res;
    res.set_reply("!");
    std::string serialized;
    res.SerializeToString(&serialized);
    server->Send(sck, serialized);
  }

private:
  std::unique_ptr<gurum::SimpleTcpServer> server{new gurum::SimpleTcpServer};
};


int main(int argc, char *argv[]) {

  GOOGLE_PROTOBUF_VERIFY_VERSION;

  const uint16_t port = 9026;
  Simple simple(port);

  std::string c;
  for(;;) {
    std::getline(std::cin, c);
  }

  google::protobuf::ShutdownProtobufLibrary();
  return 0;
}


