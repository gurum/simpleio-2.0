#include <iostream>
#include <fstream>
#include <string>
#include "addressbook.pb.h"
using namespace std;
using namespace std::placeholders;

// Iterates though all people in the AddressBook and prints info about them.
void ListPeople(const tutorial::AddressBook& address_book) {
  for (int i = 0; i < address_book.people_size(); i++) {
    const tutorial::Person& person = address_book.people(i);

    cout << "Person ID: " << person.id() << endl;
    cout << "  Name: " << person.name() << endl;
    if (person.has_email()) {
      cout << "  E-mail address: " << person.email() << endl;
    }

    for (int j = 0; j < person.phones_size(); j++) {
      const tutorial::Person::PhoneNumber& phone_number = person.phones(j);

      switch (phone_number.type()) {
        case tutorial::Person::MOBILE:
          cout << "  Mobile phone #: ";
          break;
        case tutorial::Person::HOME:
          cout << "  Home phone #: ";
          break;
        case tutorial::Person::WORK:
          cout << "  Work phone #: ";
          break;
      }
      cout << phone_number.number() << endl;
    }
  }
}

// Main function:  Reads the entire address book from a file and prints all
//   the information inside.
//int main(int argc, char* argv[]) {
//  // Verify that the version of the library that we linked against is
//  // compatible with the version of the headers we compiled against.
//  GOOGLE_PROTOBUF_VERIFY_VERSION;
//
//  if (argc != 2) {
//    cerr << "Usage:  " << argv[0] << " ADDRESS_BOOK_FILE" << endl;
//    return -1;
//  }
//
//  tutorial::AddressBook address_book;
//
//  {
//    // Read the existing address book.
//    fstream input(argv[1], ios::in | ios::binary);
//    if (!address_book.ParseFromIstream(&input)) {
//      cerr << "Failed to parse address book." << endl;
//      return -1;
//    }
//  }
//
//  ListPeople(address_book);
//
//  // Optional:  Delete all global objects allocated by libprotobuf.
//  google::protobuf::ShutdownProtobufLibrary();
//
//  return 0;
//}
























#include <simpleio/simple_client.h>
#include <simpleio/multicast_client.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>
#ifdef __APPLE__
#include <libgen.h>
#endif

using namespace gurum;
using namespace std;

#define OVERRIDE
#define _out_

class Context {
public:
  Context() : server(true) {
  }
  virtual ~Context() {
  }
  string prot; // [ tcp | udp | unix ]
  string port;
  string sckFile; // just name
  string pidFile;
  string progdir;
  bool server; // or client
};

static Context &getcontext() {
  static Context context;
  return context;
}

#define PID_PATH_PREFIX "/tmp/"

static void decode_options(int argc, char *argv[], _out_ Context &context);
static int write_pid(string &file);
static int remove_pid(string &file);
static int init_dir(const char * prefix, const char *progname);
static void deinit_dir();
static void signalhandler( int signo,siginfo_t *siginfo, void *arg);

static const char *buildDate() {
    return __DATE__ " " __TIME__;
}

static void init_sig() {
    struct sigaction sig;
  sig.sa_flags = SA_SIGINFO;
  sig.sa_sigaction = signalhandler;
  sigemptyset( &sig.sa_mask);      // No signals to be blocked when a signal is processing.
  sigfillset( &sig.sa_mask);        // All signals will be blocked.

  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &sig, 0);
  sigaction(SIGINT, &sig, 0);
  sigaction(SIGSEGV, &sig, 0);
  sigaction(SIGKILL, &sig, 0);

}

static string _progdir;
int init_dir(const char * prefix, const char *progname) {
  // pid
  string path(prefix);
  path.append(progname);
  fprintf(stderr, "%s\n", path.c_str());

  // ex: /var/run/webproxy/$webid/pid
  struct stat st;
  if(stat(path.c_str(),&st) == -1)
    ::mkdir(path.c_str(), 0777);

  path.append("/");
  path.append(to_string(getpid()));

  if(stat(path.c_str(),&st) == -1)
    ::mkdir(path.c_str(), 0777);

//  _progdir = path;
  //e.g  /tmp/exampleProgram/4452
  getcontext().progdir = path;

  string pid(getcontext().progdir);
  pid.append("/pid");
  write_pid(pid);
  return 0;
}

void deinit_dir() {
  string tmp(getcontext().progdir);
  if(! tmp.empty()) {
    tmp.append("/pid");
    fprintf(stderr, "%s will be deleted\n", tmp.c_str());
    unlink(tmp.c_str());
  }

  if( !getcontext().sckFile.empty()) {
    fprintf(stderr, "%s will be deleted\n", getcontext().sckFile.c_str());
//    unlink(getcontext().sckFile.c_str());
  }

  if( ! getcontext().progdir.empty()) {
    fprintf(stderr, "%s will be deleted\n", getcontext().progdir.c_str());
    rmdir(getcontext().progdir.c_str());
  }
}


static void usage(const char* program) {
  printf("Usage: %s <options>\n", program);
  printf("Options:\n"
      "\t-h, -help\n"
      "\t\tPrint this help\n"
      "\t-c, --client\n"
      "\t\trun as client\n"
      "\t-p, --port <1~65525>\n"
      "\t\tport. It might be a file if it is unix domain sock.\n"
      "\tex: %s tcp -p 8085\n"
      "\tex: %s unix -p /tmp/.sck\n", program ,program
  );
}

static void decode_options(int argc, char *argv[], Context &context) {
  static const char *opt_string = "p:ch";
  static struct option const longopts[] = {
      { "port", required_argument, NULL,'p' },
      { "client", no_argument, NULL,'c' },
      { "help", no_argument, NULL, 'h' },
      { NULL, 0, NULL, 0 } };

    int optc, longind=0;
    const char *name = argv[0];

    while((optc=getopt_long(argc,argv,opt_string,longopts,&longind))!=-1) {
        switch (optc)
        {
        case 'h':
          usage(name);
          exit(0);

        case 'p':
          getcontext().sckFile = optarg;
          break;

        case 'c':
          getcontext().server = false; // client
          break;

        default:
          usage(name);
          exit(0);
        }
    }

   for(int i = optind; i < argc; i++) {
      fprintf(stderr, "argv[%d]: %s\n", i, argv[i]);
      //TODO
      getcontext().prot = argv[i];
    }
}


int write_pid(string &file) {
  FILE* fp = NULL;
  fp = fopen(file.c_str(), "w");
  if(fp == NULL) {
    fprintf(stderr, "failed to open %s.", file.c_str());
    return -1;
  }
  fprintf(fp, "%d\n", getpid());
  fclose(fp);
  return 0;
}

int remove_pid(string &file) {
  if(file.empty()) return -1;

  unlink(file.c_str());
  return 0;
}

void signalhandler( int signo,siginfo_t *siginfo, void *arg) {
  sigset_t    sigset, oldset;
  sigfillset(&sigset);
  sigprocmask(SIG_BLOCK, &sigset, &oldset);

    switch(signo) {
    case SIGTERM:
    case SIGINT:
    case SIGSEGV:
    case SIGHUP:
    case SIGKILL: {
//      remove_pid(_pidfile);
      fprintf(stderr, "%s will be deleted\n", _progdir.c_str());
      deinit_dir();
      exit(1);
      break;
    }}
  sigprocmask(SIG_SETMASK, &oldset, NULL);
}



















class Simple {
public:
  Simple(uint16_t port) {
    client->SetPort(port);
    client->SetOnRead(std::bind(&Simple::OnRead, this, _1, _2));
    client->Start();
  }

  ~Simple() {

  }

  void OnRead(uint8_t *buf, int len) {
    tutorial::AddressBook address_book;
    std::string serialized;
    address_book.ParseFromArray(buf, len);

    for (int i = 0; i < address_book.people_size(); i++) {
      const tutorial::Person& person = address_book.people(i);
      std::cerr << "name: " << person.name()
          << ", email: "  << person.email( ) << std::endl;

    }
  }

private:
  std::unique_ptr<SimpleTcpClient> client{new SimpleTcpClient};
};



int main(int argc, char *argv[]) {
  fprintf(stderr,
      "%s (C)2001-2011, gurumlab \n"
  "  Built on %s\n", argv[0], buildDate());

  GOOGLE_PROTOBUF_VERIFY_VERSION;


  decode_options(argc, argv, getcontext());

  // signal
  init_sig();

  init_dir(PID_PATH_PREFIX, basename(argv[0]));

  Context &c = getcontext();

  Simple simple((uint16_t)std::stoi(c.sckFile));

  getchar();

  deinit_dir();

  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}


