/*
 * example_bind.cc
 *
 *  Created on: Jul 20, 2018
 *      Author: buttonfly
 */

#include "simpleio/netut.h"
#include <iostream>
#include <net/if.h>
#include "simpleio/simple_client.h"
#include <memory>
#include "simpleio/netut.h"
#include "log_message.h"

using namespace gurum;

static
void ShowAliases() {
  struct if_nameindex *ifs = if_nameindex();    /* retrieve the current interfaces */
  if(ifs) {
    for(struct if_nameindex *ptr = ifs;ptr->if_name; ptr++) {
      LOG(INFO) << " " << ptr->if_name << " : "
          << NetUt::dottedIP(ptr->if_name);

    }
    if_freenameindex(ifs);
  }
}

static
void usage(const char *program) {
  LOG(WARNING) << " " << program << " <remote addr> <remote port> [network alias]";
}

int main(int argc, char *argv[]) {

  ShowAliases();

  if(argc < 2) {
    usage(argv[0]);
    return 0;
  }

  std::unique_ptr<SimpleTcpClient> clnt{new SimpleTcpClient};
  clnt->SetAddr(argv[1]); // remote addr
  clnt->SetPort((uint16_t)std::stoi(argv[2])); // port
  clnt->SetTimeout(10);

  if(argv[3])
    clnt->SetAlias(argv[3]); // local network alias

  clnt->SetCallback([](){
    fprintf(stderr, "connected\n");
  },
  [&]() {
    fprintf(stderr, "disconnected\n");
    clnt->Stop();
  },
  [](int sck, int len)->bool{
    fprintf(stderr, "hook\n");
    return false; // if true, [read] wont' be called
  },
  [](uint8_t* buf, int len) {
    fprintf(stderr, "read: %s\n", buf);
  });

  if(clnt->Start() == 0) {
    string c;
    for(;;) {
      getline(cin, c);
      clnt->Send((uint8_t *)c.c_str(), c.size());
    }
  }

  clnt->Stop();
  return 0;
}
