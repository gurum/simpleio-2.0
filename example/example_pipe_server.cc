/*
 * example_pipe_server.c
 *
 *  Created on: Aug 1, 2018
 *      Author: buttonfly
 */

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/inotify.h>
#include <vector>
#include <thread>
#include <functional>
#include "log_message.h"

using OnChanged=std::function<void(int)>;

class Message {
public:

  enum action_t{
    ACTION_ENTER,
    ACTION_MOVE,
    ACTION_LEAVE
  };

  struct message_t {
    action_t action;
    int numof_points; // # of fingers
  };

  struct point_t {
    int x;
    int y;
  };

  Message(int fd, action_t action, const std::vector<struct point_t>& points) :
//  Message(int fd, action_t action) :
  fd_(fd), action_(action), points_(points){}

  ~Message() {
    message_t m;
    m.action = action_;
    m.numof_points = points_.size();
    int n;
    n = write(fd_, &m, sizeof(m));
    if(n < 0) {
      // TODO:
    }

    for(auto point : points_) {
      point_t pt;
//      pt.x = point.x();
//      pt.y = point.y();
      n  = write(fd_, &pt, sizeof(pt));
    }
  }

private:
  const int fd_;
  const action_t action_;
  const std::vector<struct point_t>& points_;
//  const ::google::protobuf::RepeatedPtrField< ::proto::TouchPoint >& points_;
};



void watch(int id, OnChanged on_changed) {
  LOG(INFO) << __func__;
#ifndef NAME_MAX
#define NAME_MAX 256
#endif

#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))
  int n;
  char buf[BUF_LEN] __attribute__ ((aligned(8)));
  n = read(id, buf, BUF_LEN);

  if(n==0) {
    // error
  }

  if(n==-1) {
    // error
  }


  char *ptr;
  struct inotify_event *event;
  for(ptr = buf; ptr < buf + n; ptr += sizeof(struct inotify_event) + event->len) {
    event = (struct inotify_event *) ptr;
    // TODO
    if (event->mask & IN_CLOSE_NOWRITE) {
      if(on_changed)
        on_changed(0);
    }
    if (event->mask & IN_OPEN)   {
      if(on_changed)
        on_changed(1);
    }
    if(event->mask & IN_CREATE) {
      LOG(INFO) << " IN_CREATE " << event->name;

    }
    if(event->mask & IN_DELETE) {
      LOG(INFO) << " IN_DELETE " << event->name;
    }
  }
}

static
int make_directory(const std::string &path) {
  for(size_t at = 0; ; ++at) {
    at = path.find("/", at);
    if(at==0) continue;

    std::string tmp = path.substr(0, at);
    fprintf(stderr, "%s[%zu]\n", tmp.c_str(), at);

    struct stat st;
    if(stat(tmp.c_str(),&st) == -1)
      ::mkdir(tmp.c_str(), 0777);

    if(at==std::string::npos) break;
  }
  return 0;
}



int main(int argc, char *argv[]) {

  int id_;
  int wd_;
  int wd_dir_;
  int fd_;
  std::thread thread_;
  bool stopped_=false;

  const char fifo_path_dir[] = "/tmp/simpleio";
  const char fifo_path[] = "/tmp/simpleio/.simple_fifo";

  make_directory(fifo_path_dir);

  unlink(fifo_path);
  int err = mkfifo(fifo_path, 0666);
  if(err) {
    LOG(ERROR) << "failed to mkfifo: " << err;
  }

  fd_ = open(fifo_path, O_RDWR);
  if(fd_ < 0) {
    LOG(ERROR) << " failed to open: " << fifo_path;
  }

  id_ = inotify_init();
  if(id_ < 0) {
    LOG(ERROR) << " failed to inotify_init: " <<  id_;
  }
  LOG(INFO) << " inotify_init";


  wd_dir_= inotify_add_watch(id_, fifo_path_dir, IN_CREATE|IN_DELETE);
  if(wd_dir_ < 0) {
    LOG(ERROR) << " failed to inotify_add_watch: " <<  wd_;
  }

  wd_ = inotify_add_watch(id_, fifo_path, IN_OPEN|IN_CLOSE);
  if(wd_ < 0) {
    LOG(ERROR) << " failed to inotify_add_watch: " <<  wd_;
  }

  LOG(INFO) << " inotify_add_watch";
  thread_ = std::thread([&](){
    while(! stopped_)  {
      watch(id_, [&](int opened){
        if(opened) {
          // open
          LOG(INFO) << " " << fifo_path << " opened";
//          fd_ = open(fifo_path, O_RDWR);
//          if(fd_ < 0) {
//            LOG(ERROR) << " failed to open: " << fifo_path;
//          }
        }
        else {
          LOG(INFO) << " " << fifo_path << " closed";
//          close(fd_);
//          fd_=-1;
        }
      });
//      sleep(1);
    }
  });

  LOG(INFO) << " press Enter key to quit";
  getchar();
  stopped_=true;

//  for(int i = 0; i < 2; i++) {
//    Message::point_t point;
//
//  }

  return 0;
}
