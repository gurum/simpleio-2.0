/*
 * example_bind.cc
 *
 *  Created on: Jul 20, 2018
 *      Author: buttonfly
 */

#include <simpleio/simple_server.h>
#include <string>
#include <memory>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>
#include "log_message.h"
#include <getopt.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <net/if.h>
#include "simpleio/netut.h"

using namespace std;
using namespace gurum;


struct CommandLine {
  bool daemon=false;
  int port=10005;
};

static int daemonize(void) {
  pid_t pid;
  if ((pid = fork()) < 0) {
    return -1;
  } else if (pid != 0) {
    exit(0);
  }
  setsid();
  umask(0);
  return 0;
}

#define _out_

static void usage(const char* program) {
  printf("Usage: %s <options>\n", program);
  printf("Options:\n"
      "\t-h, -help\n"
      "\t\tPrint this help\n"
      "\t-c, --client\n"
      "\t\trun as client\n"
      "\t-p, --port <1~65525>\n"
      "\t\tport. It might be a file if it is unix domain sock.\n"
      "\tex: %s tcp -p 8085\n"
      "\tex: %s unix -p /tmp/.sck\n", program ,program
  );
}

static void decode_options(int argc, char *argv[], _out_ CommandLine &command_line);

static
void ShowAliases() {
  struct if_nameindex *ifs = if_nameindex();    /* retrieve the current interfaces */
  if(ifs) {
    for(struct if_nameindex *ptr = ifs;ptr->if_name; ptr++) {
      LOG(INFO) << " " << ptr->if_name << " : "
          << NetUt::dottedIP(ptr->if_name);

    }
    if_freenameindex(ifs);
  }
}


int main(int argc, char *argv[]) {
  CommandLine command_line;
  decode_options(argc, argv, command_line);
  ShowAliases();
  LOG(INFO) << " port: " << command_line.port;

  if(command_line.daemon) {
    int rc;
    rc= daemonize();
    if(rc < 0) {
      return -1;
    }
  }

  unique_ptr<SimpleTcpServer> server{new SimpleTcpServer};
  server->SetTimeout(10);
  server->SetPort(command_line.port);
  server->SetCallback([](int sck){
    LOG(INFO) <<  " connected";
  },
  [] (int) {
    LOG(INFO) <<  " disconnected";
  },
  [&](int sck, uint8_t *buf, int len) {
    LOG(INFO) << " " << buf;
    LOG(INFO) << " client addr:" << NetUt::peeraddrAsString(sck);
    server->Send(buf, len);
  });

  for(;;) {
    server->ReadAndDispatch();
  }
  return 0;
}

static void decode_options(int argc, char *argv[], CommandLine &command_line) {
  static const char *opt_string = "p:dh";
  static struct option const longopts[] = {
      { "port", required_argument, NULL,'p' },
      { "daemon", no_argument, NULL,'d' },
      { "help", no_argument, NULL, 'h' },
      { NULL, 0, NULL, 0 } };

    int optc, longind=0;
    const char *name = argv[0];

    while((optc=getopt_long(argc,argv,opt_string,longopts,&longind))!=-1) {
        switch (optc)
        {
        case 'h':
          usage(name);
          exit(0);

        case 'p':
          command_line.port = std::stoi(optarg);
          break;

        case 'd':
          command_line.daemon = true;
          break;

        default:
          usage(name);
          exit(0);
        }
    }

   for(int i = optind; i < argc; i++) {
      fprintf(stderr, "argv[%d]: %s\n", i, argv[i]);
      //TODO
    }
}


