# - Try to find Protobuf
#
# The following variables are optionally searched for defaults
#  PROTOBUF_ROOT_DIR:            Base directory where all GRPC components are found
#
# The following are set after configuration is done:
#  PROTOBUF_FOUND
#  PROTOBUF_INCLUDE_DIRS
#  PROTOBUF_LIBRARIES
#  PROTOBUF_LIBRARYRARY_DIRS

include(FindPackageHandleStandardArgs)

set(PROTOBUF_ROOT_DIR "" CACHE PATH "Folder contains Google Protobuf")


find_path(PROTOBUF_INCLUDE_DIR google/protobuf/any.proto
    PATHS ${PROTOBUF_ROOT_DIR})
    
find_library(PROTOBUF_LIBRARY protobuf
	PATHS ${PROTOBUF_ROOT_DIR}
	PATH_SUFFIXES lib lib64)

find_package_handle_standard_args(PROTOBUF DEFAULT_MSG PROTOBUF_LIBRARY)

if(PROTOBUF_FOUND)
  set(PROTOBUF_INCLUDE_DIRS ${PROTOBUF_INCLUDE_DIR})
  set(PROTOBUF_LIBRARIES ${PROTOBUF_LIBRARY})
  message(STATUS "Found Protobuf   (include: ${PROTOBUF_INCLUDE_DIR}, library: ${PROTOBUF_LIBRARY})")
  mark_as_advanced(PROTOBUF_ROOT_DIR PROTOBUF_LIBRARY_RELEASE PROTOBUF_LIBRARY_DEBUG
                                 PROTOBUF_LIBRARY PROTOBUF_INCLUDE_DIR)
endif()

