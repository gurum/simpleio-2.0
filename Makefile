SHELL := /bin/bash

BUILD_ROOT ?= ${shell pwd}

VENDOR_DIR=$(BUILD_ROOT)

ifeq ($(ARCH),arm64)
TARGET_DIR?=$(BUILD_ROOT)/target.$(ARCH)
else
TARGET_DIR?=$(BUILD_ROOT)/target
endif

TARGET_INC_DIR = $(TARGET_DIR)/usr/local/include
TARGET_LIB_DIR = $(TARGET_DIR)/usr/local/lib

ifeq ($(ARCH),arm64)
CONFIG_ARGS:=-DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local -DCMAKE_SYSROOT=$(SDKTARGETSYSROOT) -DCMAKE_SYSTEM_LIBRARY_PATH=$(SDKTARGETSYSROOT)/usr/lib64
else
CONFIG_ARGS:=-DCMAKE_PREFIX_PATH=$(TARGET_DIR)/usr/local
endif

ifeq ($(ARCH),arm64)
BUILD_DIR=build.$(ARCH)
else
BUILD_DIR=build
endif

protobuf_version := 3.6.0
protobuf_tarball_name := protobuf-cpp-$(protobuf_version).tar.gz

.PHONY : build install protobuf-$(protobuf_version) 

all: configure build

protobuf-$(protobuf_version):
	@echo "[$@]"
	@if ! [ -d $@ ]; then \
		wget https://github.com/protocolbuffers/protobuf/releases/download/v$(protobuf_version)/$(protobuf_tarball_name);	\
		tar xf $(protobuf_tarball_name);	\
	fi

	cd $@ && ./autogen.sh && \
    ./configure --host=$(ARCH) --disable-shared && \
    make && make install DESTDIR=$(TARGET_DIR)
	@echo "[$@] completed"
	

protoc:
	@echo "[$@]"
	@export LD_LIBRARY_PATH=$(TARGET_LIB_DIR);	\
	$(TARGET_DIR)/usr/local/bin/protoc -I example/proto/ --cpp_out=example/proto example/proto/addressbook.proto
	$(TARGET_DIR)/usr/local/bin/protoc -I example/proto/ --cpp_out=example/proto example/proto/search.proto
	@echo "[$@] completed"


build:
	@echo "[$@]"
	cd $(VENDOR_DIR)/$(BUILD_DIR) ;	\
	$(MAKE) VERBOSE=1 ;	\
	$(MAKE) install DESTDIR=$(TARGET_DIR)

clean:
	@echo "[$@]"
	@ $(RM) -rf $(VENDOR_DIR)/$(BUILD_DIR)
	@echo "[$@] completed"

distclean: clean
	@echo "[$@]"
	@ $(RM) -rf $(TARGET_DIR)
	@echo "[$@] completed"

$(TARGET_DIR):
	@echo "[$@]"
	@mkdir -p $@
	@echo "[$@] completed"

configure:
	@echo "[$@]"
	@if ! [ -d $(VENDOR_DIR)/$(BUILD_DIR) ]; then \
		mkdir $(VENDOR_DIR)/$(BUILD_DIR);	\
		echo ${CONFIG_ARGS} ;	\
		cd $(VENDOR_DIR)/$(BUILD_DIR) && cmake ${CONFIG_ARGS} .. ; \
	fi
	@echo "[$@] completed"

install: build
	@echo "[$@]"
	@if [[ ! -z "${TARGET_BOARD_IP}" ]]; then       \
		rsync -avzh $(TARGET_DIR)/usr/local/bin/simpleio/ root@${TARGET_BOARD_IP}:/usr/local/bin/simpleio/; \
		rsync -avzh $(TARGET_DIR)/usr/local/lib/libsimpleio.so root@${TARGET_BOARD_IP}:/usr/local/lib/; \
	else    \
		echo "You need set TARGET_BOARD_IP.";   \
	fi
	@echo "[$@] completed"


adb_push:
	@echo "[$@] "
	@adb push ${TARGET_DIR}/usr/local/ /usr/local/
	@echo "[$@] completed"

