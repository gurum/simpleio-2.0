/*
 * SimpleServer.h
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#ifndef GURUM_SIMPLESERVER_H_
#define GURUM_SIMPLESERVER_H_

#include <string>
#include <thread>
#include <mutex>

#include <sys/un.h>
#include <sys/socket.h>
#include <stdint.h>
#include <netinet/in.h>
#include <functional>
#include "simpleio/config.h"
#include "simpleio/types.h"

//#ifndef OVERRIDE
//#define OVERRIDE
//#endif

namespace gurum {

class SimpleServerDelegate {

public:
	virtual void connected(int sck){}
	virtual void disconnected(int sck){}
	virtual void read(int sck, uint8_t *buf, int len){}
};

class SimpleServer {

using OnConnected=std::function<void(int)>;
using OnDisconnected=std::function<void(int)>;
using OnRead=std::function<void(int,uint8_t*,int)>;

public:
	SimpleServer();
	virtual ~SimpleServer();

	void SetOnConnected(OnConnected on_connected) {
	  on_connected_=on_connected;
	}
	void SetOnDisconnected(OnDisconnected on_disconnected) {
	  on_disconnected_=on_disconnected;
	}
	void SetOnRead(OnRead on_read) {
	  on_read_=on_read;
	}

	// @deprecated
	void SetConnectedCallback(OnConnected on_connected) {
	  on_connected_=on_connected;
	}
	// @deprecated
	void SetDisconnectedCallback(OnDisconnected on_disconnected) {
	  on_disconnected_=on_disconnected;
	}
	// @deprecated
	void SetReadCallback(OnRead on_read) {
	  on_read_=on_read;
	}

	void SetCallback(OnConnected on_connected, OnDisconnected on_disconnected, OnRead on_read) {
	  SetConnectedCallback(on_connected);
	  SetDisconnectedCallback(on_disconnected);
	  SetReadCallback(on_read);
	}

	// @deprecated
	[[deprecated]]
	void setDelegate(SimpleServerDelegate *delegate) {
		delegate_ = delegate;
	}
	virtual int Start();
	virtual void Stop();
	bool IsStarted() {
		return started_;
	}
	bool IsStopped() {
		return stopped_;
	}

	void SetTimeout(uint64_t ms) {
		timeout_ = ms;
	}

//	virtual int Send(uint8_t *buf, size_t len) {fprintf(stderr, "not implemented\n"); return -1;}
  virtual int Send(uint8_t *buf, size_t len);
  virtual int Send(const std::string &msg);
  virtual int Send(int clnt, uint8_t *buf, size_t len);
  virtual int Send(int clnt, const std::string &msg);
//	virtual int client() { return 0; }

  // You must not call Start();
  int ReadAndDispatch();

protected:
	virtual int Open()=0;
	virtual struct sockaddr *Sockaddr()=0;
	virtual size_t SockaddrLen()=0;
	void lock();
	void unlock();

protected:
	virtual void Run(int clnt);
	int ReadHead(int fd, struct simple_protocol_header &header);
	Buffer ReadBody(int fd, const struct simple_protocol_header &header);

protected:
	SimpleServerDelegate *delegate_=nullptr;
	bool stopped_=false;
	bool started_=false;
	std::thread thread_;
	std::recursive_mutex lck_;
	int sck_=-1;

	fd_set readfds_;
	int fd_max_;
	uint64_t timeout_=0;

	OnConnected on_connected_=nullptr;
	OnDisconnected on_disconnected_=nullptr;
	OnRead on_read_=nullptr;

	static int kSimpleProtocolHeaderSize;
};


class SimpleUnixServer : public SimpleServer {
public:
	SimpleUnixServer();
	virtual ~SimpleUnixServer();

	void SetUnixSockPath(const std::string &unix_sock_path) {
		unix_sock_path_ = unix_sock_path;
	}

	void Stop() override;

protected:
	int Open() override;
	struct sockaddr *Sockaddr() override;
	size_t SockaddrLen() override;

private:
	struct sockaddr_un  serveraddr_;
	struct sockaddr_un  clntaddr_;
	std::string unix_sock_path_;
};

class SimpleTcpServer : public SimpleServer {
public:
	SimpleTcpServer();
	virtual ~SimpleTcpServer();

	void SetPort(uint16_t port) {
		port_ = port;
	}

//  virtual int Send(uint8_t *buf, size_t len) override;

protected:
	virtual int Open() override;
	struct sockaddr *Sockaddr() override;
	size_t SockaddrLen() override;

protected:
	struct sockaddr_in  serveraddr_;
	struct sockaddr_in clntaddr_;
	uint16_t port_;
};

class SimpleUdpServer : public SimpleTcpServer {
public:
	SimpleUdpServer();
	virtual ~SimpleUdpServer();

protected:
	virtual int Open() override;
};

class SimplePipeServer : public SimpleServer {
public:
	SimplePipeServer();
	virtual ~SimplePipeServer();

	void SetPipePath(const std::string pipePath) {
		path_ = pipePath;
	}

	virtual int Send(uint8_t *buf, size_t len) override;

	void Stop() override;
	//@deprecated. It should not be used.
//	virtual int client() override { return _fd; }

protected:
	virtual void Run(int clnt) override;
	int Open() override;
	struct sockaddr *Sockaddr() override { return nullptr; };
	size_t SockaddrLen() override { return 0;};

private:
	std::string path_;
	int fd_=1; //default: stdout
	int id_=-1;
	int wd_=-1;
};

} /* namespace gurum */

#endif /* GURUM_SIMPLESERVER_H_ */
