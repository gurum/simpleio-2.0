/*
 * SimpleClient.h
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#ifndef GURUM_SIMPLECLIENT_H_
#define GURUM_SIMPLECLIENT_H_

#include <thread>
#include <mutex>

#include <string>
#include <functional>
#include <cstdint>
//#include <stdint.h>
#include "simpleio/config.h"
#include "simpleio/types.h"

using namespace std;

namespace gurum {

[[deprecated]]
class SimpleClientDelegate {
public:
	virtual void connected(){}
	virtual void disconnected(){}
	virtual bool hook(int sck, int len){return false;}
	virtual void read(uint8_t *buf, int len){}
};

class SimpleClient {


using OnConnected=std::function<void(void)>;
using OnDisconnected=std::function<void(void)>;
using OnHook=std::function<bool(int,int)>;
using OnRead=std::function<void(uint8_t*,int)>;

public:
	SimpleClient();
	virtual ~SimpleClient();

	[[deprecated]]
	void setDelegate(SimpleClientDelegate *delegate) {
		delegate_ = delegate;
	}

	void SetOnConnected(OnConnected on_connected) {
	  on_connected_=on_connected;
	}
	void SetOnDisconnected(OnDisconnected on_disconnected) {
	  on_disconnected_=on_disconnected;
	}
	void SetOnHook(OnHook on_hook) {
	  on_hook_=on_hook;
	}
	void SetOnRead(OnRead on_read) {
	  on_read_=on_read;
	}

	//@deprecated
	void SetConnectedCallback(OnConnected on_connected) {
	  on_connected_=on_connected;
	}
	//@deprecated
	void SetDisconnectedCallback(OnDisconnected on_disconnected) {
	  on_disconnected_=on_disconnected;
	}
	//@deprecated
	void SetHookCallback(OnHook on_hook) {
	  on_hook_=on_hook;
	}
	//@deprecated
	void SetReadCallback(OnRead on_read) {
	  on_read_=on_read;
	}

	void SetCallback(OnConnected on_connected, OnDisconnected on_disconnected, OnHook on_hook, OnRead on_read) {
	  SetConnectedCallback(on_connected);
	  SetDisconnectedCallback(on_disconnected);
	  SetHookCallback(on_hook);
	  SetReadCallback(on_read);
	}

	int Start();
	void Stop();

	int Send(void *buf, int len);
	int Send(const string &msg);
//	int Send(const char *msg);
	void SetTimeout(unsigned long ms) {
		timeout_ = ms;
	}

	// You must not call Start();
	int ReadAndDispatch();

protected:
	virtual int Open()=0;
	virtual int Recv(int fd, uint8_t *buf, size_t len);

  int ReadHead(struct simple_protocol_header &header);
  Buffer ReadBody(const struct simple_protocol_header &header);

protected:
	virtual void Run();

protected:
	bool started_=false;
	bool stopped_=false;
	std::thread thread_;
	std::mutex lck_;
	[[deprecated]]
	SimpleClientDelegate *delegate_;

	int sck_=-1;
	unsigned long timeout_;

	OnConnected on_connected_=nullptr;
	OnDisconnected on_disconnected_=nullptr;
	OnHook on_hook_=nullptr;
	OnRead on_read_=nullptr;

  static int kSimpleProtocolHeaderSize;
};

class SimpleTcpClient : public SimpleClient {
public:
	SimpleTcpClient();
	virtual ~SimpleTcpClient();
	void SetPort(uint16_t port) {
		port_ = port;
	}

	void SetAlias(const std::string &alias) {
	  alias_=alias;
	}

  void SetAddr(const std::string &addr) {
    addr_=addr;
  }

protected:
	virtual int Open();

protected:
	uint16_t port_=0;
	std::string alias_{""};
	std::string addr_{""};
};

class SimpleUdpClient : public SimpleTcpClient {
public:
	SimpleUdpClient();
	virtual ~SimpleUdpClient();

protected:
	virtual int Open();
};

class SimpleUnixClient : public SimpleClient {
public:
	SimpleUnixClient();
	virtual ~SimpleUnixClient();

	void SetUnixSockPath(const string unix_sock_path) {
		unix_sock_path_ = unix_sock_path;
	}

protected:
	virtual int Open();

private:
	string unix_sock_path_;
//	struct sockaddr_un _serveraddr;
};

class SimplePipeClient : public SimpleClient {
public:
	SimplePipeClient();
	virtual ~SimplePipeClient();

	void SetPipePath(const string path) {
		path_ = path;
	}

protected:
	virtual int Open();

private:
	string path_;
//	int _fd = 0; // default: stdin
};

} /* namespace gurum */

#endif /* APPS_SIMPLEIO_SIMPLECLIENT_H_ */
